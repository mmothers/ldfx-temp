package com.leadformix.tests;

import static org.testng.AssertJUnit.assertEquals;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.leadformix.pageobjects.HomePage;
import com.leadformix.pageobjects.LoginPage;
import com.leadformix.testutils.BrowserSetup;
import com.leadformix.testutils.CheckDriverPageState;
import com.leadformix.testutils.RetryTestCaseLogic;
import com.leadformix.testutils.TestPropertiesReader;

public class LoginTest {

	static WebDriver driver;
	static WebElement element;
	LoginPage loginpage;
	HomePage homepage;

	@BeforeClass
	public void setUp() throws Exception {
		System.out.println("..@BeforeClass-LoginTest setUp()..");
		driver = BrowserSetup.getWebDriver();
	}

	@AfterClass
	public void tearDown() throws Exception {
		System.out.println("..@AfterClass-LoginTest.tearDown()..");
		driver.quit();
	}
	
	@BeforeMethod
	public void checkBrowserState() throws MalformedURLException {
		System.out.println("..@BeforeMethod-LoginTest.checkBrowserState()..");
		if (driver.getTitle().equals("CallidusCloud Marketing Automation - Login")
				&& !CheckDriverPageState.checkIfLoginPage(driver)) {
			driver.quit();
			driver = BrowserSetup.getWebDriver();
			ITestResult result = Reporter.getCurrentTestResult();
			result.setStatus(ITestResult.FAILURE);
		}	
	}
	
	
	/*  HC-455 entered 20150303 for inconsistent login error messages
	 * 
	 */
	
	@Test(retryAnalyzer = RetryTestCaseLogic.class, invocationTimeOut = 30000, invocationCount = 1, skipFailedInvocations = true)
	public void loginTest_emptyUsernamePasswordLogin() throws InterruptedException {
		System.out.println("@Test Empty username/password displays appropriate Error mesage.");
		Reporter.log("TESTING: Expect Standard Login Error Message for Blank User/Password");
		LoginPage loginPage = new LoginPage(driver);
		loginPage.open();
		//loginPage.loginAs("", "");
		loginPage.submitLoginExpectingFailure("", "");
		assertEquals("Error: Email or Password not correct", loginPage.getErrorMessage());
		Reporter.log("PASSED - Received Expected Error Message");
	}

	
	@Test(retryAnalyzer = RetryTestCaseLogic.class, invocationTimeOut = 30000, invocationCount = 1, skipFailedInvocations = true)
	public void loginTest_invalidUsernamePasswordLogin()
			throws InterruptedException {
		System.out.println("@Test Invalid username/password displays appropriate Error mesage.");
		Reporter.log("TESTING: Expect Standard Login Error Message for Invalid User/Password");
		LoginPage loginPage = new LoginPage(driver);
		loginPage.open();
		loginPage.submitLoginExpectingFailure(TestPropertiesReader.getUserName(), "InvalidPassword");
		assertEquals("Error: Username or Password not correct", loginPage.getErrorMessage());
		Reporter.log("PASSED - Received Expected Error Message-though inconsistent HC-455");
	}
	
	
	@Test(retryAnalyzer = RetryTestCaseLogic.class, invocationTimeOut = 30000, invocationCount = 1, skipFailedInvocations = true)
	public void loginTest_isvalidUsernameinvalidPasswordLogin()
			throws InterruptedException {
		System.out.println("@Test Invalid username/password displays appropriate Error mesage.");
		Reporter.log("TESTING: Expect Standard Login Error Message for a Valid User but an Invalid Password");
		LoginPage loginPage = new LoginPage(driver);
		loginPage.open();
		loginPage.submitLoginExpectingFailure("InvalidUser", "InvalidPassword");
		assertEquals("Error: Username or Password not correct", loginPage.getErrorMessage());
		Reporter.log("PASSED - Received Expected Error Message");
	}

	
	@Test(retryAnalyzer = RetryTestCaseLogic.class, invocationTimeOut = 30000, invocationCount = 1, skipFailedInvocations = true)
	public void loginTest_longUsernamePasswordLogin()
			throws InterruptedException {
		System.out
				.println("@Test long username/password displays appropriate Error mesage.");
		Reporter.log("TESTING: Expect Standard Login Error Message for a Long User name");
		LoginPage loginPage = new LoginPage(driver);
		loginPage.open();
		loginPage.submitLoginExpectingFailure(
				"iwilluseausernamethatisreallyreallyreallylong@iwilluseausernamethatisreallyreallyreallylong.com", "password");
		assertEquals("Error: Username or Password not correct", loginPage.getErrorMessage());
		Reporter.log("PASSED - Received Expected Error Message");
	}

	
	@Test(retryAnalyzer = RetryTestCaseLogic.class, invocationTimeOut = 30000, invocationCount = 1, skipFailedInvocations = true)
	public void loginTest_specialCharUsernamePasswordLogin()
			throws InterruptedException {
		System.out
				.println("@Test specialCharacter username/password displays appropriate Error mesage.");
		Reporter.log("TESTING: Expect Standard Login Error Message when using special characters");
		LoginPage loginPage = new LoginPage(driver);
		loginPage.open();
		loginPage.submitLoginExpectingFailure("$*<>!?//user", "$*password");
		assertEquals("Error: Username or Password not correct", loginPage.getErrorMessage());
		Reporter.log("PASSED - Received Expected Error Message");
	}
	
	
	@Test(retryAnalyzer = RetryTestCaseLogic.class, invocationTimeOut = 50000, invocationCount = 1, skipFailedInvocations = true)
	public void loginTest_successfullLogin() throws InterruptedException {
		System.out.println("@Test-LoginTest.loginTest_successfullLogin()..");
		Reporter.log("TESTING: Normal Login");
		LoginPage loginPage = new LoginPage(driver);
		loginPage.open();
		loginPage.loginAs(
						TestPropertiesReader.getUserName(),
						TestPropertiesReader.getUserPassword());
		
		HomePage homePage = new HomePage(driver);
		assertEquals("CallidusCloud - Dashboard", homePage.getTitle());
		
		assertEquals("Logged in as " + TestPropertiesReader.getUserName(), homePage.getLoggedInUser());
		
		homePage.logout(); //note that if assert occurs then this won't execute and thus leaves other login tests at bad starting point - need to fix this
		Reporter.log("PASSED - Logged in successfully");
	}
	
	@Test(retryAnalyzer = RetryTestCaseLogic.class, invocationTimeOut = 50000, invocationCount = 1, skipFailedInvocations = true)
	public void loginTest_TrialRedirect() throws InterruptedException {
		System.out.println("@Test-LoginTest.loginTest_TrialRedirect()..");
		Reporter.log("TESTING: Trial signup redirect");
		LoginPage loginPage = new LoginPage(driver);
		loginPage.open();
		
		assertEquals("New to CallidusCloud Marketing Automation? Try us for free",driver.findElement(loginPage.newToCallidusLocator).getText());
		driver.findElement(loginPage.newToCallidusLocator).click();
		
		Thread.sleep(3000); //fails check below if no sleep (should create waitForElement or Page method...)
	    assertEquals("CallidusCloud Marketing Automation Free Trial", loginPage.getTitle());
	   
	    loginPage.loginClick(); // take us back to login page to leave in a good state
	    Reporter.log("PASSED - Trial signup page redirect successfull");
	}
	
}


