package com.leadformix.testutils;

//import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryTestCaseLogic { //implements IRetryAnalyzer {
	private int retryCount = 0;
	private int maxRetryCount = 0;

	public boolean retry(ITestResult result) {

		if (retryCount < maxRetryCount) {
			retryCount++;
			return true;
		}
		return false;
	}
}
