package com.leadformix.testutils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CheckDriverPageState
{

	public static boolean checkIfLoginPage(WebDriver driver) 
	{
		System.out.println("..CheckDriverPageState.checkIfLoginPage()..");
		By userNameLocator = By.id("form_login"); 
		if (waitForElementPresent(driver, userNameLocator, 4) != null)  
			return true;
		else
			return false;
	}

	public static WebElement waitForElementPresent(WebDriver driver,final By by, int timeOutInSeconds) 
	{
		System.out.println("..CheckDriverPageState.waitForElementPresent()..");
		WebElement element;
		try 
		{
			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			return element;
		} 
		catch (Exception e)
		{
			System.out.println("The Previous Test - Got Stuck! :'(");
		}
		return null;
	}
}
