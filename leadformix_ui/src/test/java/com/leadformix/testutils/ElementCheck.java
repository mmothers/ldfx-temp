package com.leadformix.testutils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

public class ElementCheck 
{
	private static final TimeUnit SECONDS = null;

	/*
	 * Models a condition that might reasonably be expected to eventually
	 * evaluate to something that is neither null nor false. Examples : Would
	 * include determining if a web page has loaded or that an element is
	 * visible.
	 * 
	 * Note that it is expected that ExpectedConditions are idempotent. They
	 * will be called in a loop by the WebDriverWait and any modification of the
	 * state of the application under test may have unexpected side-effects.
	 * 
	 * WebDriverWait will be used as we used in the Expected conditions code
	 * snippet as above. sleeper is something same as the Thread.sleep() method,
	 * but this with an Abstraction around the thread.sleep() for better
	 * testability
	 */
	// WebDriverWait wait = new WebDriverWait(driver, 10);
	// WebElement element =
	// wait.until(ExpectedConditions.elementToBeClickable(By.id(>someid>)))

	/**
	 * Clear Field before taking Input
	 * 
	 * @param driver
	 * @param elementLocator
	 * @param element_wait
	 * @return
	 * @throws InterruptedException
	 */
	public static void clearFieldsBeforeInput(WebDriver driver,By elementLocator, int element_wait) throws InterruptedException 
	{
		System.out.println("..ElementCheck.clearFieldsBeforeInput()..");
		ElementCheck.presenceOfElement(driver, elementLocator, element_wait).clear();
    }

	/**
	 * 
	 * @param driver
	 * @param elementLocator
	 * @param element_wait2
	 * @return
	 */
	public static WebElement presenceOfElement(WebDriver driver,final By elementLocator, int element_wait) 
	{
		System.out.println("..ElementCheck.presenceOfElement()..");
		WebElement dynamicElement = new WebDriverWait(driver, element_wait).until(ExpectedConditions.presenceOfElementLocated(elementLocator));
		return dynamicElement;
	}

	/**
	 * Each FluentWait instance defines the maximum amount of time to wait for a
	 * condition, as well as the frequency with which to check the condition.
	 * Furthermore, the user may configure the wait to ignore specific types of
	 * exceptions whilst waiting, such as NoSuchElementExceptions when searching
	 * for an element on the page.
	 * 
	 */
	public static WebElement waitForElement(WebDriver driver,final By elementLocator) 
	{
		System.out.println("..ElementCheck.waitForElement()..");
		// Copied from Fluent Wait Example:
		// Waiting 30 seconds for an element to be present on the page, checking
		// for its presence once every 5 seconds.
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(30, SECONDS).pollingEvery(5, SECONDS).ignoring(NoSuchElementException.class);
        WebElement foo = wait.until(new Function<WebDriver, WebElement>() 
        	{
			    public WebElement apply(WebDriver driver) 
			{
				  return driver.findElement(elementLocator);
			}
		
       });

		return foo;
	}

	/**
	 * Waits for the element to be clickable
	 * 
	 * @param driver
	 * @param elementLocator
	 * @param waitPeriod
	 */

	public static WebElement waitForElementToBeClickable(WebDriver driver,final By elementLocator, int element_wait) 
	{
		System.out.println("..ElementCheck.waitForElementToBeClickable()..");
		WebElement dynamicElement = new WebDriverWait(driver, element_wait).until(ExpectedConditions.elementToBeClickable(elementLocator));
		return dynamicElement;
	}

	/**
	 * Waits for Element to be Visible
	 * 
	 * @param driver
	 * @param elementLocator
	 * @param waitPeriod
	 */
	public static WebElement waitForElementToBeVisible(WebDriver driver,final By elementLocator, int element_wait)
	{
		System.out.println("..ElementCheck.waitForElementToBeVisible()..");
		WebElement dynamicElement = new WebDriverWait(driver, element_wait).until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
		return dynamicElement;
	}
}
