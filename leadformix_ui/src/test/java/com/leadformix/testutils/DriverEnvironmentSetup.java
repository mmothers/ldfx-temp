package com.leadformix.testutils;

import java.io.File;

public class DriverEnvironmentSetup 
{
	public static void main(String[] args) 
	{
		System.out.println("..DriverEnvironmentSetup.main()..");
		FileDownloader fileDownloader = new FileDownloader();

		String driverPath = new String("C:/selenium/drivers/");
		String addOnPath = new String("C:/selenium/firebug/");

		String chromeDriverAddress = new String("http://chromedriver.storage.googleapis.com/2.9/chromedriver_win32.zip");
		String ieDriverAddress = new String("http://selenium-release.storage.googleapis.com/2.40/IEDriverServer_Win32_2.40.0.zip");

		String firebugAddress = new String("https://getfirebug.com/releases/firebug/1.11/firebug-1.11.4.xpi");

		boolean seleniumDriverPathCheck = (new File("C:/selenium/drivers/")).mkdirs();
		boolean addonDriverPathCheck = (new File("C:/selenium/firebug/")).mkdirs();

		FileUnzip fileUnzip = new FileUnzip();

		if (seleniumDriverPathCheck && addonDriverPathCheck) 
		{
			// Download Chrome and IEDriver form the URL incase they are not
			// present
			fileDownloader.fileDownload(chromeDriverAddress, driverPath);
			fileUnzip.unzipFile("C:/selenium/drivers/chromedriver_win32.zip",driverPath);
			fileDownloader.fileDownload(ieDriverAddress, driverPath);
			fileUnzip.unzipFile("C:/selenium/drivers/IEDriverServer_Win32_2.40.0.zip",driverPath);
			fileDownloader.fileDownload(firebugAddress, addOnPath);
		}
		else 
		{
			System.out
					.println("Try Again! Path was not created or is already present.");
		}
	}
}
