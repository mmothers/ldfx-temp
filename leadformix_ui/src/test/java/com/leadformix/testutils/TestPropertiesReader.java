package com.leadformix.testutils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestPropertiesReader
{

    static String configFilePath = new String("lf.properties");
    static Properties properties = new Properties();
    static InputStream input = null;

    public static String propertiesReader(String propertyKey)
    {
    	System.out.println("..TestPropertiesReader.propertersReader()..");
    	String propertyValue = new String();

	try
	{
	    input = new FileInputStream(configFilePath);
	    properties.load(input);

	    propertyValue = properties.getProperty(propertyKey);

	} 
	catch (IOException ex) {
	    ex.printStackTrace();
	} 
	finally 
	{
	    if (input != null) {
		try
		{
		    input.close();
		} 
		catch (IOException e) {
		    e.printStackTrace();
		}
	    }
	}

	return propertyValue;
    }

    public static String getTestEnvironmentPath()
    {
    	System.out.println("..TestPropertiesReader.getTestEnvironmentPath()..");
    	return new String(propertiesReader("testEnvironmentPath"));
    }

    public static String getRemoteMachineURL() 
    {
	return new String(propertiesReader("remoteMachineURL"));
    }

    public static String getBrowserTypeProperty()
    {
	return new String(propertiesReader("browserType"));
    }

    public static String getPhantomDriverLocation()
    {
	return new String(propertiesReader("phantomDriverLocation"));
    }

    public static String getFirefoxDriverLocation() 
    {
	return new String(propertiesReader("firefoxDriverLocation"));
    }
    
    public static String getIEDriverLocation()
    {
	return new String(propertiesReader("ieDriverLocation"));
    }

    public static String getChromeDriverLocation() 
    {
	return new String(propertiesReader("chromeDriverLocation"));
    }

    public static String getFireBugPath()
    {
	return new String(propertiesReader("firebugFirefoxPath"));
    }
    
    public static String getUserName() 
    {
    	System.out.println("..TestPropertiesReader.getUserName()..");
    	return new String(propertiesReader("username"));
    }
    
    public static String getUserName2() 
    {
    	System.out.println("..TestPropertiesReader.getUserName()..");
    	return new String(propertiesReader("username2"));
    }
    
    public static String getUserPassword() 
    {
    	System.out.println("..TestPropertiesReader.getUserPassword()..");
    	return new String(propertiesReader("password"));
    }
}