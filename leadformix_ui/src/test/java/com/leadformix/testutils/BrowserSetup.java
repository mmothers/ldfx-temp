package com.leadformix.testutils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;

public class BrowserSetup 
{

    /*
     * Browser Types: Firefox, IE9, IE10, IE11, Chrome (Drivers for these
     * browsers are machine specific and path to them can be set in this file.)
     */
    public enum BrowserType 
    {
	FIREFOX, IE9, IE10, IE11, CHROME, PHANTOM
    }

    private static String ieDriverLocation = TestPropertiesReader.getIEDriverLocation();
    private static String chromeDriverLocation = TestPropertiesReader.getChromeDriverLocation();
    private static String firebugPath = TestPropertiesReader.getFireBugPath();
    private static String remoteMachineURL = TestPropertiesReader.getRemoteMachineURL();
    private static String phantomLocation = TestPropertiesReader.getPhantomDriverLocation();;

    /**
     * Gets the browser from the configuration file
     * 
     * @return
     */
    private static BrowserType getBrowser() 
    {
    	System.out.println("..BrowserSetup.getBrowser()..");
    	BrowserType browserType = null;
	    browserType = BrowserType.valueOf(TestPropertiesReader
		.getBrowserTypeProperty());
	    //browserType = BrowserType.valueOf("PHANTOM");
	    return browserType;
    }

    /**
     * 
     */
    private static WebDriver getPhantomDriver()
    {
	System.setProperty("phantomjs.binary.path", phantomLocation);
	DesiredCapabilities phatomCapabilities = new DesiredCapabilities();
	phatomCapabilities.setJavascriptEnabled(true);
	phatomCapabilities.setCapability("loadImages", true);
	phatomCapabilities.setCapability("takesScreenshot", false);
	phatomCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});  
	WebDriver driver = new PhantomJSDriver(phatomCapabilities);
	return driver; 
    }
    /**
     * Chrome Driver: Get the chrome driver with desired capabilities.
     * 
     * @return
     * @throws MalformedURLException
     */
    protected static WebDriver getChromeDriver() throws MalformedURLException 
    {
	System.setProperty("webdriver.chrome.driver", chromeDriverLocation);

	DesiredCapabilities chromeCapabilities = DesiredCapabilities.chrome();
	WebDriver driver = new ChromeDriver(chromeCapabilities);
	//WebDriver driver = new RemoteWebDriver(new URL(remoteMachineURL),chromeCapabilities);
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
	return driver;
    }

    /**
     * Firefox Driver (Set all the Firefox capabilities here for Firefox)
     * 
     * @return
     * @throws MalformedURLException
     **/
    protected static WebDriver getFirefoxDriver() throws MalformedURLException 
    {

	FirefoxProfile profile = new FirefoxProfile();
	System.out.println("..BrowserSetup.getFirefoxDriver()..");
	// try {
	// profile.addExtension(new File(firebugPath));
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// System.out.println("FirebugPath not correct @ " + firebugPath);
	// e.printStackTrace();
	// }

	/*
	 * Setting firebug preferences here!
	 */
	// profile.setEnableNativeEvents(true);
	// profile.setPreference("extensions.firebug.currentVersion", "1.11.4");
	// profile.setPreference("extensions.firebug.defaultPanelName",
	// "console");
	// profile.setPreference("extensions.firebug.net.enableSites", true);
	// profile.setPreference("extensions.firebug.allPagesActivation", "on");
	// profile.setPreference("extensions.firebug.showFirstRunPage", false);

	// Capabilities to set the firefox download preferences
	profile.setPreference("browser.download.folderList", 2);
	profile.setPreference("browser.download.manager.showWhenStarting",
		false);

	//profile.setPreference("browser.download.dir", downloadPath);
	// profile.setPreference("browser.helperApps.alwaysAsk.force", false);
	profile.setPreference(
		"browser.helperApps.neverAsk.openFile",
		"application/octet-stream, text/html, application/xhtml+xml, application/xml, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	// MIME Types: (Excel): application/vnd.ms-excel, application/msexcel,
	// application/x-msexcel, application/x-ms-excel,
	// application/vnd.ms-excel, application/x-excel,
	// application/x-dos_ms_excel, application/xls, application/xlsx,

	profile.setPreference(
		"browser.helperApps.neverAsk.saveToDisk",
		"application/octet-stream, text/html, application/xhtml+xml, application/xml, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

	// profile.setPreference("extensions.firebug.netexport.defaultLogDir",
	// "/tmp");
	// profile.setPreference("extensions.firebug.netexport.saveFiles",
	// true);
	// profile.setPreference("extensions.firebug.netexport.alwaysEnableAutoExport",
	// true);

	// WebDriver driver = new FirefoxDriver(profile);

	DesiredCapabilities desiredCapabilities = DesiredCapabilities.firefox();
	desiredCapabilities.setCapability(FirefoxDriver.PROFILE, profile);
	//WebDriver driver = new FirefoxDriver(desiredCapabilities);
	
	// Note that if RemoteWebDriver used a selenium server must be running (local or remote)
	WebDriver driver = new RemoteWebDriver(new URL(remoteMachineURL),desiredCapabilities);
	//driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
	return driver;
    }

    /**
     * IE Driver (Set all the capabilities here for IE)
     * 
     * @return
     * @throws MalformedURLException
     */

    protected static WebDriver getIEDriver() throws MalformedURLException
    {
	System.setProperty("webdriver.ie.driver", ieDriverLocation);

	DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
	ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
	ieCapabilities.setCapability("ensureCleanSession", true);
	ieCapabilities.setCapability("requireWindowFocus", true);
	ieCapabilities.setCapability("enablePersistentHover", false);

	WebDriver driver = new InternetExplorerDriver(ieCapabilities);
	// WebDriver driver = new RemoteWebDriver(new URL(remoteMachineURL),ieCapabilities);
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
	return driver;
    }

    /**
     * Gets the type of webdriver from the enum list based on the configuration
     * file
     * 
     * @return
     * @throws MalformedURLException
     */
    public static WebDriver getWebDriver() throws MalformedURLException 
    {
    	System.out.println("..BrowserSetup.getWebDriver()..");
    WebDriver driver;
	BrowserType browserType = BrowserSetup.getBrowser();
	// TODO kill browser threads -- before getting a new one

	switch (browserType)
	{
	case FIREFOX:
	    driver = getFirefoxDriver();
	    break;
	case IE9:
	    driver = getIEDriver();
	    break;
	case CHROME:
	    driver = getChromeDriver();
	    break;
	case PHANTOM:
	    driver = getPhantomDriver();
	    break;
	default:
	    driver = getFirefoxDriver();
	    break;
	}
	return driver;
    }
}
