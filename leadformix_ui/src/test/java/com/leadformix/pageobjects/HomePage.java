package com.leadformix.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;

//Possibly looking to remove this HomePage and move items to BasePage.

public class HomePage extends BasePage {

	By logoutLinkLocator = By.name("pic5"); 
	By loggedInUserLocator = By.cssSelector("div.logo");
	
//	private static By globalNavBoxLocator = By.id("GlobalNav");
//  private static By marketingActivities = By.id("Marketing");
    
    public HomePage(WebDriver driver) {
    	System.out.println("..HomePage Constructor..");
	    this.driver = driver;    
    }
    
    public HomePage logout() throws InterruptedException {
		//driver.switchTo().defaultContent();  //An abstraction allowing the driver to access the browser's history and to navigate to a given URL.
		
		new Actions(driver).moveToElement(driver.findElement(logoutLinkLocator)).build().perform();
        driver.findElement(logoutLinkLocator).click();
		//Thread.sleep(10000);
		return new HomePage(driver);
	}    
    
    public String getLoggedInUser()
    {
	WebElement myDynamicElement = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(loggedInUserLocator));
	return driver.findElement(loggedInUserLocator).getText();
    }
    
       
}
