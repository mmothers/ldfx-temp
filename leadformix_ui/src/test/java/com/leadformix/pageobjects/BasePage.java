package com.leadformix.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.leadformix.testutils.TestPropertiesReader;

public class BasePage {
    WebDriver driver;

    // TOP MENU NAVIGATION LOCATORS
    // Dashboard
    static By dashboardLocator = By.id("Dashboard");
    // MyWebsite
    static By myWebSiteLocator = By.id("Website");
    static By seoLocator = By.linkText("SEO");
    static By leadHooksLocator = By.linkText("LeadHooks");
    static By chatLocator = By.linkText("Chat");
    static By blogLocator = By.linkText("Blog");
    // MarketingActivities
    static By markingActivitiesLocator = By.id("Marketing");
    static By emailsLocator = By.linkText("Emails");
    static By landingPagesLocator = By.linkText("Landing Pages");
    static By workflowLocator = By.linkText("Workflow");
    static By campaignsLocator = By.linkText("Campaigns");
    static By scoringLocator = By.linkText("Scoring");
    static By formsLocator = By.linkText("Forms");
    static By ulrShortenerLocator = By.linkText("URL Shortener");
    static By urlBuilderLocator = By.linkText("URL Builder"); 
    // List Management
    static By listManagementLocator = By.id("ListMgnt");
    static By visitsLocator = By.linkText("Visits");
    static By customFiltersLocator = By.linkText("Custom Filters");		
    static By contactsLocator =	By.linkText("Contacts");
    static By emailListsLocator = By.linkText("Email Lists");
    /*  ToDo
    // Marketing Assets
    static By marketingAssetsLocator = 
    static By landingPageTemplatesLocator = 
    static By emailTemplatesLocator = 
    static By workflowTemplatesLocator = 
    static By emailListsLocator = 
    // Social
    static By socialLocator = 
    // Sales Enablement
    static By salesEnablementLocator =  
    static By emailSignatureLocator =  
    static By tickerLocator =  
    static By mobileAppsLocator = 
    // Reports
    static By reportsLocator =  
    static By campaignReportsLocator =  
    static By sentEmailReportsLocator =  
    static By emailReportsLocator = 
    static By abTestReportsLocator =  
    static By formResponseReportsLocator = 
    static By rssSentEmailReportsLocator = 
    static By workflowReportsLocator = 
    static By visitReportsLocator = 
    static By webTrafficReportsLocator = 
    static By callTrackingReportsLocator = 
    // Setup
    static By setupLocator = 
*/    
    
    
    //======================================================================================================
    //APPLICATION URL (Read from the properties file)
    //======================================================================================================
    public String url =	TestPropertiesReader.getTestEnvironmentPath();

    
    
    public void open() 
    {
    	System.out.println("..PageBase.open()..");
    	driver.get(url);
    }

    public String getTitle() 
    {
	return driver.getTitle();
    }
    
    

}
