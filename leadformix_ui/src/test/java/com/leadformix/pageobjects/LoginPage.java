package com.leadformix.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {
    
	public By usernameLocator = By.id("form_login");
	public By passwordLocator = By.id("form_password");
	public By loginButtonLocator = By.className("MrT");
	public By errorLocator = By.className("error");
    public By lostPasswordLocator = By.linkText("Lost my password");
    public By newToCallidusLocator = By.linkText("New to CallidusCloud Marketing Automation? Try us for free");
    public By loginLinkLocator = By.linkText("LOGIN");
    
    public LoginPage(WebDriver driver)
    {
	    System.out.println("..LoginPage constructor..");
    	this.driver = driver;

    }

    public LoginPage typeUsername(String username) throws InterruptedException
    {
	    driver.findElement(usernameLocator).sendKeys(username);
	    Thread.sleep(1000);
	    return this;
    }

    public LoginPage typePassword(String password) throws InterruptedException
    {
	    driver.findElement(passwordLocator).sendKeys(password);
	    return this;
    }

    public LoginPage submitLogin() throws InterruptedException
    {
    	System.out.println("..LoginPage.submitLogin()..");
    	driver.findElement(loginButtonLocator).submit();
	    return new LoginPage(driver);
    }

    public LoginPage submitLoginExpectingFailure(String username,String password) throws InterruptedException
    {
    	System.out.println("..LoginPage submitLoginExpectingFailure()..");
    	typeUsername(username);
	    typePassword(password);
	    driver.findElement(loginButtonLocator).submit();
	    return new LoginPage(driver);
    }
  
        
    public LoginPage loginAs(String username, String password) throws InterruptedException
    {
    	System.out.println("..LoginPage loginAs()..");
    	typeUsername(username);
	    typePassword(password);
	    return submitLogin();
    }

    public String getErrorMessage()
    {
    	System.out.println("..LoginPage getErrorMessage()..");
    	WebElement myDynamicElement = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(errorLocator));
	    return driver.findElement(errorLocator).getText();
    }
    
    public LoginPage loginClick() throws InterruptedException {
		//driver.switchTo().defaultContent();  //An abstraction allowing the driver to access the browser's history and to navigate to a given URL.
		
		new Actions(driver).moveToElement(driver.findElement(loginLinkLocator)).build().perform();
		
        driver.findElement(loginLinkLocator).click();
		
		return new LoginPage(driver);
	} 
    
    public LoginPage loginPageSelect(String username,
			String password) throws InterruptedException {
		typeUsername(username);
		typePassword(password);
		//WebElement myDynamicElement = (new WebDriverWait(driver, 10))
		//		.until(ExpectedConditions
		//				.presenceOfElementLocated(loginButtonLocator));
		driver.findElement(loginButtonLocator).click();
		return new LoginPage(driver);
	}
}
